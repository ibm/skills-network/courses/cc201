<img src="images/IDSN.png" width="200">


<div align="center"> <b>Cheat Sheet: OpenShift CLI</b>
</div>


<table>
<tr>
<th width="30%">Command</th width="70%"><th>Description</th>
</tr>

<tr>
<td width="30%"><b>oc get</b></td>
<td width="70%">Displays a resource.
</tr>


<tr>
<td width="30%"valign="top"><b>oc project</b></td>
<td width="70%">Switches to a different project.
</td>

</tr>

<tr>
<td width="30%"valign="top"><b>oc version</b></td>
<td width="70%">Displays version information
</td>
</tr>


</table>

## <h3 align="center"> © IBM Corporation 2022. All rights reserved. <h3/>
